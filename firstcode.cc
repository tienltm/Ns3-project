#include <ns3/core-module.h>
// #include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/config-store.h>
#include <ns3/lte-module.h>
#include <ns3/buildings-helper.h>
#include <ns3/applications-module.h>
#include <ns3/point-to-point-helper.h>

using namespace ns3;

int main (int argc, char *argv[])
{
	double simTime = 1.1;
	double distance = 60.0;
	uint8_t ueNo =6;
	uint8_t enBNo=1;

	CommandLine cmd;
	cmd.Parse(argc, argv);
	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults ();
	cmd.Parse(argc, argv);
// this comment line for testing the conflict
// the rest of the simulation program follows
	Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
//	lteHelper->SetSchedulerType("ns3::RrFfMacScheduler");
//	 lteHelper->EnableLogComponents ();
//Create Node objects for the eNB(s) and the UEs:
	NodeContainer enbNodes;
	enbNodes.Create (enBNo);
	NodeContainer ueNodes;
	ueNodes.Create (ueNo);
//Configure the Mobility model for all the nodes:
	 Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
		// positionAlloc->Add (Vector(distance *))
	for (int i = 1; i < ueNo+2; i++){
		positionAlloc->Add (Vector(distance * i, 0, 0));
	}
	MobilityHelper mobility;
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.SetPositionAllocator(positionAlloc);
	mobility.Install(enbNodes);
	mobility.Install(ueNodes);

	BuildingsHelper::Install (enbNodes);
	BuildingsHelper::Install (ueNodes);
//Install an LTE protocol stack on the eNB(s):
	NetDeviceContainer enbDevs;
	enbDevs = lteHelper->InstallEnbDevice (enbNodes);
//Install an LTE protocol stack on the UEs:
	NetDeviceContainer ueDevs;
	ueDevs = lteHelper->InstallUeDevice (ueNodes);
//Attach the UE to the eNb, create RRC connection
	lteHelper->Attach (ueDevs, enbDevs.Get (0));

//Activate a data radio bearer between each UE and the eNB
	enum EpsBearer::Qci q = EpsBearer::GBR_GAMING;
	EpsBearer bearer (q);
	lteHelper->ActivateDataRadioBearer (ueDevs, bearer);
	lteHelper->EnableTraces ();
//Set Timer
	Simulator::Stop (Seconds (simTime));
//Run simulator
	Simulator::Run ();
//Clean up & destroy
	Simulator::Destroy ();
	return 0;
//this comment line for testing head
}



